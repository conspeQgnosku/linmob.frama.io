+++
title = "Weekly GNU-like Mobile Linux Update (7/2023): New Hardware and new PinePhone Keyboard Firmware"
draft = false
date = "2023-02-19T22:10:00Z"
[taxonomies]
tags = ["Sailfish OS", "GNOME Camera", "Mobian", "Tokodon","Morph Browser","PinePhone Keyboard","Librem 5","Volla","Lapdock",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

_Also:_ A fix for PinePhone Mesa troubles, a new Manjaro Phosh beta, a new postmarketOS splashscreen, a guide to developing mobile apps with a nested Phosh session, a new Tokodon release and an association Peter might join._

<!-- more -->
_Commentary in italics._

### Hardware
- Liliputing: [Auspicious Machine is a modular handheld Linux PC with a BlackBerry-style keyboard and gaming buttons](https://liliputing.com/auspicious-machine-is-a-modular-handheld-linux-pc-with-a-blackberry-style-keyboard-and-gaming-buttons/)
- Liliputing: [Volla Phone X23 is a rugged phone that runs Google-free Android or Ubuntu Touch - Liliputing](https://liliputing.com/volla-phone-x23-is-a-rugged-phone-that-runs-google-free-android-or-ubuntu-touch/)

### Software progress

#### PinePhone Keyboard Firmware
- megi's PinePhone Development Log: [Pinephone keyboard firmware update (1.3)](https://xnux.eu/log/#078). _Massive improvements! Due to this, I am [playing](https://fosstodon.org/@linmob/109891673052579480) with the keyboard again. [How do you make it work for you, which tweaks did you apply?](https://fosstodon.org/@linmob/109890646904069393)_

#### GNOME ecosystem
- This Week in GNOME: [#83 Sharing Is Caring](https://thisweek.gnome.org/posts/2023/02/twig-83/)
- Base-Art: [WebRTC in WebKitGTK and WPE, status updates, part I](https://base-art.net/Articles/webrtc-in-webkitgtk-and-wpe-status-updates-part-i/)
- [Alexander Mikhaylenko: "libadwaita updates So a few things happened since the last thread …" - Neurodifferent Me](https://neurodifferent.me/@alexm/109878726853369050). _Massive thread about new stuff coming to libadwaita!_

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: a smooth release of Plasma 5.27](https://pointieststick.com/2023/02/17/this-week-in-kde-a-smooth-release-of-plasma-5-27/)
- KDE Announcements: [Plasma 5.27](https://kde.org/announcements/plasma/5/5.27.0/)
- Carl Schwan: [Tokodon 23.02.0 release](https://carlschwan.eu/2023/02/17/tokodon-23.02.0-release/)
- KDE Eco: [KDE Eco Handbook: "Applying The Blue Angel Criteria To Free Software"](https://eco.kde.org/blog/2023-02-14_handbook-announcement/)
- dot.KDE.org: [Akademy 2023 Call for Proposals is Now Open!](https://dot.kde.org/2023/02/13/akademy-2023-call-proposals-now-open)
- Phoronix: [KDE Plasma 6.0 Sees More Feature Work, Improvements For Dolphin & Plasma 5.27.1 Fixes](https://www.phoronix.com/news/KDE-Plasma-5.27-Release-Week)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch Q&A 122](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-122-3877)

#### Sailfish OS
- Nick Cartron: [Jolla and Sailfish OS at FOSDEM 23](https://www.ncartron.org/jolla-and-sailfish-os-at-fosdem-23.html)

#### Distributions
- Manjaro PinePhone Phosh: [Beta 29](https://github.com/manjaro-pinephone/phosh/releases/tag/beta29)
- Strits: [Plasma Mobile 5.27 + PlaMo Gear 23.01.0](https://blog.strits.dk/plasma-mobile-5-27-plamo-gear-23-01-0/)
- Manjaro Blog: [FOSDEM 2023](https://blog.manjaro.org/fosdem-2023/)
- Phoronix: [Debian 12 "Bookworm" Enters Its Soft Freeze](https://www.phoronix.com/news/Debian-12-Soft-Freeze)
- Phoronix: [Fedora Planning Ahead For The Next 5 Years](https://www.phoronix.com/news/Fedora-5-Year-Plan-2023)

#### Linux
- Phoronix: [Linux 6.3 Supports The Snapdragon 8 Gen 2 & Other New High-End Arm SoCs](https://www.phoronix.com/news/Linux-6.3-Arm-SoC-Updates)

#### Non-Linux
- Lup Yuen: [NuttX RTOS for PinePhone: Exploring USB](https://lupyuen.github.io/articles/usb2)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-02-17](https://matrix.org/blog/2023/02/17/this-week-in-matrix-2023-02-17)
- Matrix.org: [Synapse 1.77 released](https://matrix.org/blog/2023/02/16/synapse-1-77-released)
- Matrix.org: [Matrix v1.6 release](https://matrix.org/blog/2023/02/14/matrix-v-1-6-release)

### Worth noting
#### Events and associations
- Upcoming events: [Linux App Summit, April 21st - 23rd, Brno, Czech Republic.](https://floss.social/@gnome/109833722419928471)
- [Sailfish OS News Network on Mastodon](https://mastodon.social/@sailfishosnews/109875439133793910): "Members of the #SailfishOS community created the "Sailmate association". Sailmate is a non-profit association aiming to support and promote free and open source mobile operating system alternatives. Among others, the association aims to provide help (financially and technically) to projects related to #SailfishOS.
  - Website: [https://sailmates.net/](https://sailmates.net/)
  - Read more about it: [https://forum.sailfishos.org/t/join-th](https://forum.sailfishos.org/t/join-the-sailmates-association/14627)"

#### Software
- An important fix for PinePhone/AllWinner A64 users has landed in mesa: [lima: don't use resource\_from\_handle while creating scanout (e6f8d7ed) · Commits · Erico Nunes / mesa · GitLab](https://gitlab.freedesktop.org/enunes/mesa/-/commit/e6f8d7ede5f80e906074f09a78dbcedafb3ef0e1?action=show&controller=projects%2Fcommit&id=e6f8d7ede5f80e906074f09a78dbcedafb3ef0e1&namespace_id=enunes&project_id=mesa&w=1).
- [postmarketOS: "A year in the making, today we merged our new boot #splashscreen! A fancy animation, some text allowing you to show off to your friends the #mainline kernel you run, it's all included!…"](https://fosstodon.org/@postmarketOS/109887055754137919)
- [Pine Guild: "Morph Browser (from @ubports Ubuntu Touch) in @mobian with #phosh? Yep. ...and it works quite snappy (even on @PINE64 PinePhone).…"](https://mastodon.social/@pineguild/109885383760258437)
- [Thomas Perl: "Introducing the Maemulator, because I'm sure you missed playing that 2009 tech demo on your modern Linux-based machine…"](https://dosgame.club/@thp/109870952164453340)
- [Sebastian Krzyszkowiak: "Audio over DisplayPort over USB-C, coming soon to a #Librem5 near you.…" - Librem Social](https://social.librem.one/@dos/109864781767105179)
- [@linmob: "I took a few photos with Pinhole running on #Mobian on my #PinePhonePro today - great work everybody! …"](https://fosstodon.org/@linmob/109864981569473099) _I deployed the latest Mobian installer image today, and it includes this as a default camera app!_

### Worth reading
#### Hardware hands-on
- IEEE Spectrum: [Hardcore Hands-On: This Pocket PC Is Big on Open Source - IEEE Spectrum](https://spectrum.ieee.org/mnt-pocket-reform)

#### Nested Phosh for fun and development
- Phosh: [Developing for Mobile Linux with Phosh - Part 0: Running nested](https://phosh.mobi/posts/phosh-dev-part-0/). _Really helpful! BTW: Make sure to check out that lovely new phosh.mobi site!_

#### PinePhone experience
- Valhalla's Things: [My experience with a PinePhone](https://blog.trueelena.org/blog/2023/02/15-my-experience-with-a-pinephone/index.html)

#### Flatpak
- Linux Phone Guides: [The Flatpak Controversy: Part 1 (Storage)](https://linuxphoneguides.ovh/index.php/2023/02/18/the-flatpak-controversy-part-1-storage/)
- Linux Phone Guides: [The Flatpak Controversy: Part 2 (Speed)](https://linuxphoneguides.ovh/index.php/2023/02/18/the-flatpak-controversy-part-2-speed/). _Nice posts, I shared [my thoughts on the fediverse](https://fosstodon.org/@linmob/109885237074983873)._

#### Librem 5
- Chris Vogel: [Measuring power consumption on the Librem5](https://chrichri.ween.de/articles/00c9759/measuring-power-consumption-on-the-librem5). _Helpful!_
- Purism: [Announcing the Lapdock Kit](https://puri.sm/posts/announcing-the-lapdock-kit/). _This is a good move, and Purism's addition make sense - these laptops seem to be designed for desk use only OOTB. That said, in most cases, a used laptop will be way more useful, and the only downside will be that it's yet another computer to manage._

#### Be nice, people!
- Akselmo: [FOSS communities: You don’t need to yell](https://www.akselmo.dev/2023/02/17/You-dont-need-to-yell.html)

### Worth watching
- Human Rights Tech (RTP): [I2P Browser For Pinephone / Linux Devices (ready to try)](https://www.youtube.com/watch?v=qB_Co3DgYzE)
- emde: [Out-Of-Band with Pinephone](https://www.youtube.com/watch?v=zXR6hkyDF2g)
- emde: [Out-Of-Band Commuication with Pinephone](https://www.youtube.com/watch?v=_jBv2UXt464)


### Thanks

Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

