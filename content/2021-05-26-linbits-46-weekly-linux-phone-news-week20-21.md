+++
title = "LinBits 46: Weekly Linux Phone news / media roundup (week 20/21)"
aliases = ["2021/05/26/linbits46-weekly-linux-phone-news-week2021.html"]
date = "2021-05-26T21:07:00Z"
updated = "2021-05-26T22:30:00Z"
[taxonomies]
tags = ["PINE64", "PinePhone", "LINMOBapps", "postmarketOS",  "Mobian", "Jumpdrive", "Maemo Leste", "PineTalk", "Sxmo",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note = "Added the Sxmo theming video I originally forgot."
+++

_It's Wednesday. Now what happened since that Sunday ages ago I did this last?_

PinePhone kernel improvements and accessory news, a Mobian progress report, postmarketOS turns four years old and more.<!-- more --> _Commentary in italics._


### Software development and releases
* [Squeekboard 1.14.0](https://forums.puri.sm/t/squeekboard-1-14-0-released/13517), the GNOME on Mobile virtual keyboard, has seen another release.
* [Calls 0.3.3](https://source.puri.sm/Librem5/calls/-/commit/31219632206fa83c95cd9d76377272c26b6a5e70) is also out, read the changelog.
* [Jumpdrive](https://github.com/dreemurrs-embedded/Jumpdrive) is getting [support for Snapdragon 845 devices](https://fosstodon.org/@calebccff/106264870758038993 ), namely the Xiaomi Pocophone F1 and the OnePlus 6(T).
* [DanctNIX Arch Linux ARM has seen another release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210522) featuring the usual version bumps (which this time contain notable kernel improvements) plus a switch back to ext4 for all the people who keep buying bad SD cards.
* [Manjaro Phosh Beta 10](https://forum.manjaro.org/t/manjaro-arm-beta10-with-phosh-pinephone/67332) brings new releases of software and uses Pipewire now among other things.

### Worth reading 
#### PinePhone Kernel news
* xnux.eu: [PinePhone kernel news](https://xnux.eu/log/#033). _A progress report by Megi on his PinePhone kernel progress: Power savings, a time travel fix and more._
  * Linux Smartphones: [Megi’s PinePhone kernel updates bring battery life, performance improvements](https://linuxsmartphones.com/megis-pinephone-kernel-updates-bring-battery-life-performance-improvements/). _Brad's take._


#### Distribution progress reports and announcements
* Mobian: [2021-05-17 Status update](https://blog.mobian-project.org/posts/2021/05/17/update-2021-05-17/). _Impressive update, go read it!_
  * Linux Smartphones: [Mobian begins porting its Debian-based OS to more smartphones and tablets](https://linuxsmartphones.com/mobian-begins-porting-its-debian-based-os-to-more-smartphones/). _Brad's take on Mobian's announcement, focusing on the new devices Mobian supports now._
* postmarketOS: [Four years of postmarketOS / AlpineConf 2021](https://postmarketos.org/blog/2021/05/26/four-years/). _Four years of postmarketOS, wow, congratulations! If you have not watched these AlpineConf talks yet, you can now do so with the Q&A at the end._
* Maemo Leste: [IRC channel migration](https://maemo-leste.github.io/maemo-leste-irc-channel-migration.html). _Just a heads up, #maemo-leste is now on irc.libera.chat_
* Purism: [What’s new in PureOS 10](https://puri.sm/posts/whats-new-in-pureos-10/). _News about Byzantium._

#### Nemo Mobile progress
* Linux Smartphones: [Nemo Mobile brings Glacier UX to the PinePhone and other Linux handhelds](https://linuxsmartphones.com/nemomobile-brings-glacier-ux-to-the-pinephone-and-other-linux-handhelds/). _Great post about Nemo Mobile by Brad. I have been looking into it, and it's still rough, but there's tons of potential. If you are looking to forward to help out a project, please do consider Nemo Mobile._

#### Plasma Corner
* Carl Schwan: [LAS 2021 and improvements in the applications infrastructure](https://carlschwan.eu/2021/05/18/las-2021-and-improvements-in-the-applications-infrastructure/). _Nice improvements to Plasma release tooling._
* Bhushan Shah: [Moving on](https://blog.bshah.in/2021/05/17/moving-on/). _I wish Bhushan all the best for his new position and hope he will still find time for Plasma Mobile every then and now!_

#### App Garden
* PineGuild: [Telegrand – a lightweight Telegram client that integrates well with Phosh](https://pineguild.com/telegrand-a-lightweight-telegram-client-that-integrates-well-with-phosh/)
* Janet's Shenanigans: [A Week In Tok](https://blog.blackquill.cc/a-week-in-tok). _Tok is WIP Kirigami app for Telegram._

#### Hardware corner
* PineGuild: [Extension cases for PinePhone are working!](https://pineguild.com/extensions-case-for-pinephone-are-working/) _Nice development!_
* PineGuild: [PinePhone Keyboard: first impression is very positive](https://pineguild.com/pinephone-keyboard-first-impression-is-very-positive/). _An interview with Lukasz Erecinski about the upcoming clamshell-style keyboard accessory._

### Worth listening
* PineTalk [009: Eventful No. 9](https://www.pine64.org/podcast/009-eventful-no-9/). _Ezra and I managed to put out another episode for you to listen to. We discussed Lora among other things. Have fun!_

### Worth watching

#### Highlight
* Erfan Abdi: [Hands on new Anbox on ubports](https://www.youtube.com/watch?v=-mDiOrrFCgU). _Impressive progress. Looks like this is basically running full Lineage OS next to Ubuntu Touch. Note that this a Halium device running an Android kernel, and the project is named [anbox-halium](https://xray2000.gitlab.io/anbox/). I hope to find the time to have a look at this on my Yggdrasil device (GS290) soon._ 

#### Hardware keyboard corner
* Martijn Braam: [First look at the PinePhone Keyboard dev unit](https://spacepub.space/videos/watch/9e4b8d1c-eb67-4243-9277-94d4e7cf7255). _Nice first look at a keyboard case developer unit by Martijn!
* Lukasz Erecinski: [Messing with PinePhone keyboard prototype](https://www.youtube.com/watch?v=6suKlsPiVDw). _Early look at the hardware by Lukasz._

#### Home Automation
* ICHT: [LCARS Home Automation System (pinephone)](https://www.youtube.com/watch?v=MtQzmspOlGs)

#### Software corner
* Drew Naylor 2: [RetiledStart prototype running on the PinePhone](https://www.youtube.com/watch?v=NpUnrb1wC_8). _A new development: A Windows Phone style launcher for the PinePhone written in Python. You can find the code on [GitHub](https://github.com/DrewNaylor/Retiled)._
* Jozef Mlich: [Nemomobile based on Manjaro on PinePhone - May 2021](https://www.youtube.com/watch?v=uH50ODeRMdY). _Nice to see some progress with Nemo Mobile!_
* HackersGame: [Librem 5 Screen Reader](https://odysee.com/@HackersGame:4/librem-5-screen-reader:5). _HackersGame has built a fun screen reader. You can find the source on [Purism's gitlab](https://source.puri.sm/david.hamner/readit)._

##### Impressions subsection

* Elatronion: [PinePhone Manjaro KDE First Impressions](https://odysee.com/@Elatronion:a/PinePhoneManjaroKDEFirstImpressions:d). _My buddy Ezra finally made another video! Check it out!_
* HyperVegan: [First Impressions of Pinephone with postmarketOS Phosh](https://odysee.com/@HyperVegan:2/20210521_Pinephone_PostmarketOS_Impressions_720p:6). 
* Avisando: [PinePhone Megapixels Test (2021)](https://www.youtube.com/watch?v=NFDt9C0vWEc). _Megapixels 1.0.1 in action! Also: Yay, Avisando is back :)_
* Low Orbit Flux: [PinePhone – Linux Smart Phone - Beta Edition with Convergence Package - Running Manjaro and Plasma](https://www.youtube.com/watch?v=u--8omwZBwQ). _1 hour of early PinePhone impressions!_

#### Sxmo Theming
Nephitejnf: [The Worst Pinephone Theme, Based on the Worst Videogame](https://www.youtube.com/watch?v=ssabBcugpiU). 

#### Does not work

* Tamás Vándorffy: [Maemo Leste Booting on Nokia N9 16gbyte phone](https://www.youtube.com/watch?v=M3QsREPH7Is). _Tamás tried it so you don't have to do that yet ;-)_
* Twinntech: [My New Pinephone](https://www.youtube.com/watch?v=9JmDYyD-k0k). _Not everybody can be happy._


### Stuff I did

#### Content
Aside from last weeks [PineTalk episode](https://www.pine64.org/podcast/009-eventful-no-9/), I managed to record and publish two videos, both dealing with non-PinePhone devices, but maybe still interesting for PinePhone users, as one demoes pmbootstrap and the second one is not just about the Librem 5, but also about the Linux phone app ecosystem:

* Installing postmarketOS with Plasma Mobile on the Motorola Moto G4 Play (Harpia) (May 21st, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/c3d3cbf0-e3c5-43ed-86a0-84d90c0ba9bc), [Odysee](https://odysee.com/@linmob:3/installing-postmarketos-with-plasma:0), [YouTube](https://www.youtube.com/watch?v=C4LKllQT_GQ).

* Librem 5 with PureOS Byzantium: Work in Progress + AppStream Metadata nerdery (May 22nd, 2021): [DevTube](https://devtube.dev-wiki.de/videos/watch/9ccf19be-92f9-4096-907f-eeace8f11563), [Odysee](https://odysee.com/@linmob:3/librem-5-with-pureos-byzantium-work-in:a), [YouTube](https://www.youtube.com/watch?v=zEsu9GFGzns).

I also published a blog post today about [the installation of Biktorgj's modem firmware](https://linmob.net/flashing-biktorgjs-modem-firmware/) and a post on Sunday explaining why [LinBits is now a Wednesday thing](https://linmob.net/linbits-is-changing-new-day/).

#### LINMOBapps
I did a little less work on LINMOBapps this week. It's replacement now has a [Twitter account](https://twitter.com/linuxphoneapps). Two new colums have been added, detailing the main project programming language and the build system, information targeted mainly at developers and distributors, filling of these columns will likely take a while. I also added the following apps this week: 
* [Emulsion](https://github.com/lainsce/emulsion), a color palette manager, and
* [Curtail](https://github.com/Huluti/Curtail), an image compressor.

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
